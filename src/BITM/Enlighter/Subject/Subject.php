<?php
namespace App\Subject;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
use PDOException;

class Subject extends DB{
    public $id="";
    public $sub_name="";
    public $group_id="";
    public $optional_subject="";
    public $group_subject="";
    public $common_subject="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL)
    {
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("sub_name",$postVariable)){
            $this->sub_name=$postVariable['sub_name'];
        }
        if(array_key_exists("group_name",$postVariable)){
            $this->group_id=$postVariable['group_name'];
        }
        if(array_key_exists("cat_id",$postVariable)){
            if ($postVariable['cat_id'] == 'common')
            {
                $this->common_subject='Yes';
            }
            else
            {
                $this->common_subject='No';
            }
            if ($postVariable['cat_id'] == 'group')
            {
                $this->group_subject='Yes';
            }
            else
            {
                $this->group_subject='No';
            }
            if ($postVariable['cat_id'] == 'optional')
            {
                $this->optional_subject='Yes';
            }
            else
            {
                $this->optional_subject='No';
            }
        }

    }

    public function store()
    {
                $arrData = array($this->sub_name,$this->common_subject,$this->group_subject,$this->group_id,$this->optional_subject);
                $sql = "INSERT into subject_info(subject_name,is_common,is_group_subject,group_id,is_optional)VALUES (?,?,?,?,?)";
                $STH = $this->DBH->prepare($sql);
                $result = $STH->execute($arrData);

                if ($result) {
                    Message::message("Success!Data has been inserted Successfully :)");
                } else {
                    Message::message("Falied!Data has not been inserted Successfully :(");
                }

                Utility::redirect('create.php');


    }


    public function index()
    {
            $sql = "SELECT * from subject_info WHERE is_deleted='No' ORDER BY subject_id DESC";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $arrAllData = $STH->fetchAll();
            return $arrAllData;

    }

    public function view(){
        $sql="SELECT * from subject_info where subject_id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }


    public function update(){
        $arrData = array($this->sub_name,$this->common_subject,$this->group_subject,$this->group_id,$this->optional_subject);

        $sql="UPDATE subject_info SET subject_name=?,is_common=?,is_group_subject=?,group_id=?,is_optional=? WHERE subject_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("list_view.php");

    }


    public function delete(){
        $sql="DELETE from subject_info WHERE subject_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");
    }

    public function delete_from_trash(){
        $sql="DELETE from subject_info WHERE subject_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }




    public function trash(){
        $sql="UPDATE subject_info SET is_deleted=NOW() WHERE subject_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");

    }

    public function trash_list(){
        $sql="SELECT * from subject_info WHERE is_deleted<>'No' ORDER BY subject_id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
    }

    public function recover(){
        $sql="UPDATE subject_info SET is_deleted='No' WHERE subject_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from subject_info  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from subject_info  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }


    public function search($requestArray)
    {
    $sql = "";
    if (isset($requestArray['byClass']) && isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND (class_name LIKE '%" . $requestArray['search'] . "%' OR profile_picture LIKE '%" . $requestArray['search'] . "%')";
    if (isset($requestArray['byClass']) && !isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND class_name LIKE '%" . $requestArray['search'] . "%'";
    if (!isset($requestArray['byClass']) && isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND class_name LIKE '%" . $requestArray['search'] . "%'";

    $STH = $this->DBH->query($sql);
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $allData = $STH->fetchAll();

    return $allData;
    }

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM subject_info WHERE is_deleted ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->subject_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->subject_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function loadtable()
    {
        $sql = "SELECT * from class_info WHERE is_deleted='No' ORDER BY class_id DESC  ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

    public function className($id){
        $sql="SELECT * from class_info where class_id=$id";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }
    public function loadSubject()
    {
        //var_dump($id);
        $sql = "SELECT * from subject_info WHERE  is_deleted='No' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }
    public function loadSubjectName($id1)
    {
        //var_dump($id);
        $sql = "SELECT * from subject_info WHERE  subject_id=$id1 ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }


}

?>


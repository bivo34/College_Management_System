<?php
namespace App\Model;
use PDO;
use PDOException;

class Database {
    public $DBH;
    public $dbname="college_management_system";
    public $host="localhost";
    public $username="root";
    public $password="";
    public function __construct()
    {
        try
        {
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username,$this->password);
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            //echo "connection successfuly";
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}
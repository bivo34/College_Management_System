<?php

namespace App\Teacher;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Teacher extends DB
{
    public $id;
    public $subject_id;
    public $teacher_name;
    public $father_name;
    public $mother_name;
    public $spouse_name;
    public $birth_date;
    public $joining_date;
    public $nationality;
    public $religion;
    public $NID;
    public $contact_number;
    public $present_address;
    public $permanent_address;
    public $profile_photo;

    public function __construct()
    {
        parent::__construct();
        /*if(!isset($_SESSION))
            session_start();*/
    }

    public function setData($postVaribaleData=NULL)
    {
       if(array_key_exists("id",$postVaribaleData))
       {
           $this->id = $postVaribaleData['id'];
       }
        if(array_key_exists("subject_id",$postVaribaleData))
        {
            $this->subject_id = $postVaribaleData['subject_id'];
        }
        if(array_key_exists("teacher_name",$postVaribaleData))
        {
            $this->teacher_name = $postVaribaleData['teacher_name'];
        }
        if(array_key_exists("father_name",$postVaribaleData))
        {
            $this->father_name = $postVaribaleData['father_name'];
        }
        if(array_key_exists("mother_name",$postVaribaleData))
        {
            $this->mother_name = $postVaribaleData['mother_name'];
        }
        if(array_key_exists("spouse_name",$postVaribaleData))
        {
            $this->spouse_name = $postVaribaleData['spouse_name'];
        }
        if(array_key_exists("birth_date",$postVaribaleData))
        {
            $this->birth_date = $postVaribaleData['birth_date'];
        }
        if(array_key_exists("joining_date",$postVaribaleData))
        {
            $this->joining_date = $postVaribaleData['joining_date'];
        }
        if(array_key_exists("nationality",$postVaribaleData))
        {
            $this->nationality = $postVaribaleData['nationality'];
        }
        if(array_key_exists("religion",$postVaribaleData))
        {
            $this->religion = $postVaribaleData['religion'];
        }
        if(array_key_exists("NID",$postVaribaleData))
        {
            $this->NID = $postVaribaleData['NID'];
        }
        if(array_key_exists("contact_number",$postVaribaleData))
        {
            $this->contact_number = $postVaribaleData['contact_number'];
        }
        if(array_key_exists("present_address",$postVaribaleData))
        {
            $this->present_address = $postVaribaleData['present_address'];
        }
        if(array_key_exists("permanent_address",$postVaribaleData))
        {
            $this->permanent_address = $postVaribaleData['permanent_address'];
        }
        if(array_key_exists("profile_photo",$postVaribaleData))
        {
            $this->profile_photo = $postVaribaleData['profile_photo'];
        }

    }//end of set data

    public function store()
    {
        $subject = implode(',', $this->subject_id);
         $religion1 = implode(',', $this->religion);
        $folder = "/xampp/htdocs/College_Management_System/TeachersPhoto/";
        $image_name=$_FILES['profile_photo']['name'];
        $path = $folder . time() . $image_name;
        $save_path = time() . $image_name;
        $temporary_location = $_FILES['profile_photo']['tmp_name'];
        move_uploaded_file($temporary_location, $path);
        $arrData = array($subject,$this->teacher_name,$this->father_name,$this->mother_name,$this->spouse_name,$this->birth_date,$this->joining_date,$this->nationality,$religion1,$this->NID,$this->contact_number,$this->present_address,$this->permanent_address,$save_path);

        $sql = "INSERT into teacher_info(subject_id,teacher_name,father_name,mother_name,spouse_name,birth_date,joining_date,nationality,religion,nid,contact_number,present_address,permanent_address,photo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('create.php');

    }

    public function index(){

        $STH = $this->DBH->query("SELECT * from teacher_info WHERE is_deleted = 'No' ORDER BY id ASC");


        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }

    public function view(){

        $sql = 'SELECT * from teacher_info where id='.$this->id ;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view();
    public function update()
    {  $subject = implode(',', $this->subject_id);
        $religion1 = implode(',', $this->religion);
        $folder = "/xampp/htdocs/College_Management_System/TeachersPhoto/";
        $image_name=$_FILES['profile_photo']['name'];
        $path = $folder . time() . $image_name;
        $save_path = time() . $image_name;
        $temporary_location = $_FILES['profile_photo']['tmp_name'];
        move_uploaded_file($temporary_location, $path);
        $arrData = array($subject,$this->teacher_name,$this->father_name,$this->mother_name,$this->spouse_name,$this->birth_date,$this->joining_date,$this->nationality,$religion1,$this->NID,$this->contact_number,$this->present_address,$this->permanent_address,$save_path);

        $sql = "UPDATE teacher_info SET subject_id=?,teacher_name = ?,father_name = ?,mother_name = ?,spouse_name = ?,
 birth_date = ?,joining_date = ?,nationality = ?,religion = ?,nid = ?,contact_number = ?,present_address = ?,
 permanent_address = ?,photo = ? WHERE id = ".$this->id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("list_view.php");

    }
    public function trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE teacher_info SET is_deleted = NOW()  WHERE teacher_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash

    public function trash_view($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from teacher_info WHERE is_deleted <> 'No'  ORDER BY teacher_id DESC");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashview();
    public function recover()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE teacher_info SET is_deleted = 'No' WHERE teacher_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
    public function delete()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from teacher_info WHERE teacher_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash
    public function delete_from_trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from teacher_info WHERE teacher_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from teacher_info  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();

    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from teacher_info  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();

   /*public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT * FROM `teacher_info` WHERE `is_deleted` ='0'
                    AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";

        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) )
            $sql = "SELECT * FROM `teacher_info` WHERE `is_deleted` ='0' AND `book_title` LIKE '%".$requestArray['search']."%'";

        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )
            $sql = "SELECT * FROM `teacher_info` WHERE `is_deleted` ='0' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `teacher_info` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        //$STH = $this->DBH->query($sql);
        // $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

*/
}//end of book title class
<?php
namespace App\Student;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Student extends DB
{
    public $std_id;
    public $image;
    public $first_name;
    public $last_name;
    public $birth_day;
    public $fathers_name;
    public $fathers_profession;
    public $mothers_name;
    public $mothers_profession;
    public $nationality;
    public $gender;
    public $religion;
    public $contact_no1;
    public $contact_no2;
    public $present_address;
    public $permanent_address;
    public $school_name;
    public $ssc_group;
    public $ssc_result;
    public $ssc_roll;
    public $ssc_registration;
    public $user_name;
    public $password;
    public $confirm_password;
    public $email;
    public $roll_no;
    public $class_id;
    public $group_id;
    public $approved;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariable = null)
    {

        if (array_key_exists("id", $postVariable)) {
            $this->std_id = $postVariable['id'];
        }
        if (array_key_exists("image", $postVariable)) {
            $this->image = $postVariable['image'];
        }
        if (array_key_exists("first_name", $postVariable)) {
            $this->first_name = $postVariable['first_name'];
        }
        if (array_key_exists("last_name", $postVariable)) {
            $this->last_name = $postVariable['last_name'];
        }
        if (array_key_exists("birth_day", $postVariable)) {
            $this->birth_day = $postVariable['birth_day'];
        }
        if (array_key_exists("fathers_name", $postVariable)) {
            $this->fathers_name = $postVariable['fathers_name'];
        }
        if (array_key_exists("fathers_profession", $postVariable)) {
            $this->fathers_profession = $postVariable['fathers_profession'];
        }
        if (array_key_exists("mothers_name", $postVariable)) {
            $this->mothers_name = $postVariable['mothers_name'];
        }
        if (array_key_exists("mothers_profession", $postVariable)) {
            $this->mothers_profession = $postVariable['mothers_profession'];
        }
        if (array_key_exists("nationality", $postVariable)) {
            $this->nationality = $postVariable['nationality'];
        }
        if (array_key_exists("gender", $postVariable)) {
            $this->gender = $postVariable['gender'];
        }
        if (array_key_exists("religion", $postVariable)) {
            $this->religion = $postVariable['religion'];
        }
        if (array_key_exists("contact_no1", $postVariable)) {
            $this->contact_no1 = $postVariable['contact_no1'];
        }
        if (array_key_exists("contact_no2", $postVariable)) {
            $this->contact_no2 = $postVariable['contact_no2'];
        }
        if (array_key_exists("present_address", $postVariable)) {
            $this->present_address = $postVariable['present_address'];
        }
        if (array_key_exists("permanent_address", $postVariable)) {
            $this->permanent_address = $postVariable['permanent_address'];
        }
        if (array_key_exists("school_name", $postVariable)) {
            $this->school_name = $postVariable['school_name'];
        }
        if (array_key_exists("ssc_group", $postVariable)) {
            $this->ssc_group = $postVariable['ssc_group'];
        }
        if (array_key_exists("ssc_result", $postVariable)) {
            $this->ssc_result = $postVariable['ssc_result'];
        }
        if (array_key_exists("ssc_roll", $postVariable)) {
            $this->ssc_roll = $postVariable['ssc_roll'];
        }
        if (array_key_exists("ssc_registration", $postVariable)) {
            $this->ssc_registration = $postVariable['ssc_registration'];
        }
        if (array_key_exists("user_name", $postVariable)) {
            $this->user_name = $postVariable['user_name'];
        }
        if (array_key_exists("password", $postVariable)) {
            $this->password = md5($postVariable['password']);
        }
        if (array_key_exists("confirm_password", $postVariable)) {
            $this->confirm_password = md5($postVariable['confirm_password']);
        }
        if (array_key_exists("email", $postVariable)) {
            $this->email = $postVariable['email'];
        }
        if (array_key_exists("roll_no", $postVariable)) {
            $this->roll_no = $postVariable['roll_no'];
        }
        if (array_key_exists("class_id", $postVariable)) {
            $this->class_id = $postVariable['class_id'];
        }
        if (array_key_exists("group_id", $postVariable)) {
            $this->group_id = $postVariable['group_id'];
        }
        $this->approved = 'Yes';
    }

    public function store()
    {
        $religion = implode(',', $this->religion);
        $folder = "/xampp/htdocs/College_Management_System/img/";
        $image_name=$_FILES['image']['name'];
        $path = $folder . time() . $image_name;
        $save_path = time() . $image_name;
        $temporary_location = $_FILES['image']['tmp_name'];
        move_uploaded_file($temporary_location, $path);

        $arrayData = array($save_path, $this->first_name, $this->last_name,
            $this->birth_day, $this->fathers_name, $this->fathers_profession,
            $this->mothers_name, $this->mothers_profession, $this->nationality,
            $this->gender, $religion, $this->contact_no1, $this->contact_no2,
            $this->present_address, $this->permanent_address, $this->school_name, $this->ssc_group,
            $this->ssc_result, $this->ssc_roll, $this->ssc_registration,$this->user_name,$this->password,$this->confirm_password,$this->email);

        $sql = "insert into student_info(profile_pic,first_name,last_name,birth_date,fathers_name,fathers_profession,mothers_name,mothers_profession,nationality,gender,religion,contact_no1,contact_no2,present_address,permanent_address,school_name,ssc_group,ssc_result,ssc_roll,ssc_registration,user_name,password,confirm_password,email)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrayData);
        if ($result)
            Message::message("Data has been inserted Successfully :)");
        else
            Message::message("Failure ....Data is not inserted (:");
        Utility::redirect('admission_form.php');
    }
    public  function index()
    {
        $STH = $this->DBH->query("SELECT * from student_info where is_deleted='no'");

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from student_info where std_id='.$this->std_id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update(){
        $religion = implode(',', $this->religion);
        $folder = "/xampp/htdocs/College_Management_System/img/";
        $image_name=$_FILES['image']['name'];
        $path = $folder . time() . $image_name;
        $save_path = time() . $image_name;
        $temporary_location = $_FILES['image']['tmp_name'];
        move_uploaded_file($temporary_location, $path);
        $arrayData = array($save_path, $this->first_name, $this->last_name,
            $this->birth_day, $this->fathers_name, $this->fathers_profession,
            $this->mothers_name, $this->mothers_profession, $this->nationality,
            $this->gender, $religion, $this->contact_no1, $this->contact_no2,
            $this->present_address, $this->permanent_address, $this->school_name, $this->ssc_group,
            $this->ssc_result, $this->ssc_roll, $this->ssc_registration,$this->user_name,
            $this->password,$this->confirm_password,$this->email);

        $sql="UPDATE student_info SET profile_pic=?,first_name=?, last_name=?,birth_date=?,fathers_name=?,
fathers_profession=?,mothers_name=?,mothers_profession=?,nationality=?,gender=?,
religion=?,contact_no1=?,contact_no2=?,present_address=?,permanent_address=?,school_name=?,ssc_group=?,ssc_result=?,
ssc_roll=?,ssc_registration=?,user_name=?,password=?,confirm_password=?,email=? WHERE std_id=".$this->std_id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrayData);
        Utility::redirect("list_view.php");

    }
    public function approval(){
        $religion = implode(',', $this->religion);
        $folder = "/xampp/htdocs/College_Management_System/img/";
        $image_name=$_FILES['image']['name'];
        $path = $folder . time() . $image_name;
        $save_path = time() . $image_name;
        $temporary_location = $_FILES['image']['tmp_name'];
        move_uploaded_file($temporary_location, $path);
        $arrayData = array($save_path, $this->first_name, $this->last_name,
            $this->birth_day, $this->fathers_name, $this->fathers_profession,
            $this->mothers_name, $this->mothers_profession, $this->nationality,
            $this->gender, $religion, $this->contact_no1, $this->contact_no2,
            $this->present_address, $this->permanent_address, $this->school_name, $this->ssc_group,
            $this->ssc_result, $this->ssc_roll, $this->ssc_registration,$this->user_name,
            $this->password,$this->confirm_password,$this->email,$this->roll_no,$this->class_id,$this->group_id,$this->approved);

        $sql="UPDATE student_info SET profile_pic=?,first_name=?, last_name=?,birth_date=?,fathers_name=?,
fathers_profession=?,mothers_name=?,mothers_profession=?,nationality=?,gender=?,
religion=?,contact_no1=?,contact_no2=?,present_address=?,permanent_address=?,school_name=?,ssc_group=?,ssc_result=?,
ssc_roll=?,ssc_registration=?,user_name=?,password=?,confirm_password=?,email=?,roll_no=?,class_id=?,group_id=?,is_approved=? WHERE std_id=".$this->std_id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrayData);
        Utility::redirect("list_view.php");

    }

    public function trash(){
        $sql="UPDATE student_info SET is_deleted=NOW() WHERE std_id=".$this->std_id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");

    }

    public function trash_list(){
        $sql="SELECT * from student_info WHERE is_deleted<>'No' ORDER BY std_id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }

    public function recover(){
        $sql="UPDATE student_info SET is_deleted='No' WHERE std_id=".$this->std_id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }
    public function delete(){
        $sql="DELETE from student_info WHERE std_id=".$this->std_id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");
    }

    public function delete_from_trash(){
        $sql="DELETE from student_info WHERE std_id=".$this->std_id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from student_info  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from student_info  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function search($requestArray)
    {
        $sql = "";
        if (isset($requestArray['byName']) && isset($requestArray['byRoll'])) $sql = "SELECT * FROM student_info WHERE is_deleted ='No' AND (first_name LIKE '%" . $requestArray['search'] . "%' OR ssc_roll LIKE '%" . $requestArray['search'] . "%')";
        if (isset($requestArray['byName']) && !isset($requestArray['byRoll'])) $sql = "SELECT * FROM student_info WHERE is_deleted ='No' AND first_name LIKE '%" . $requestArray['search'] . "%'";
        if (!isset($requestArray['byName']) && isset($requestArray['byRoll'])) $sql = "SELECT * FROM student_info WHERE is_deleted ='No' AND ssc_roll LIKE '%" . $requestArray['search'] . "%'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM student_info WHERE is_deleted ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->first_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->first_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function loadStudent($id)
    {
        //var_dump($id);
        $sql = "SELECT * from student_info WHERE  is_deleted='No' AND student_info.class_id  = $id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }
    public function loadStudentName($id1){

        $sql = "SELECT * from student_info where std_id= $id1";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
}
?>

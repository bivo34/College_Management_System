<?php
namespace App\Result;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Result extends DB
{
    public $class_id;
    public $exam_id;
    public $student_id;
    public $subject_id;
    public $marks;
    public $grade_point;
    public $grade;


    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariable = null)
    {

        if (array_key_exists("id", $postVariable)) {
            $this->id = $postVariable['id'];
        }
        if (array_key_exists("class_id", $postVariable)) {
            $this->class_id = $postVariable['class_id'];
        }
        if (array_key_exists("exam_id", $postVariable)) {
            $this->exam_id = $postVariable['exam_id'];
        }
        if (array_key_exists("student_id", $postVariable)) {
            $this->student_id = $postVariable['student_id'];
        }
        if (array_key_exists("subject_id", $postVariable)) {
            $this->subject_id = $postVariable['subject_id'];
        }
        if (array_key_exists("marks", $postVariable)) {
            $this->marks = $postVariable['marks'];
        }
        if($this->marks >= 0 && $this->marks <=32)
        {
            $this->grade = 'F';
            $this->grade_point = '0.00';
        }
        else if($this->marks >= 33 && $this->marks <=39)
        {
            $this->grade = 'D';
            $this->grade_point = '1.00';
        }
        else if($this->marks >= 40 && $this->marks <=49)
        {
            $this->grade = 'C';
            $this->grade_point = '2.00';
        }
        else if($this->marks >= 50 && $this->marks <=59)
        {
            $this->grade = 'B';
            $this->grade_point = '3.00';
        }
        else if($this->marks >= 60 && $this->marks <=69)
        {
            $this->grade = 'A-';
            $this->grade_point = '3.50';
        }
        else if($this->marks >= 70 && $this->marks <=79)
        {
            $this->grade = 'A';
            $this->grade_point = '4.00';
        }
        else
        {
            $this->grade = 'A+';
            $this->grade_point = '5.00';
        }
    }

    public function store()
    {

        $arrayData = array($this->exam_id,$this->class_id,$this->student_id, $this->subject_id, $this->marks,$this->grade_point,$this->grade);

        $sql = "insert into student_result(exam_id,class_id,student_id,subject_id,marks,grade_point,grade)VALUES (?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrayData);
        if ($result)
            Message::message("Data has been inserted Successfully :)");
        else
            Message::message("Failure ....Data is not inserted (:");
        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->DBH->query("SELECT * from student_result where is_deleted='no'");

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from student_result where result_id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update(){

        $arrayData = array($this->exam_id,$this->class_id,$this->student_id, $this->subject_id, $this->marks,$this->grade_point,$this->grade);

        $sql="UPDATE student_result SET exam_id=?,class_id=?, student_id=?,subject_id=?,marks=?,
        grade_point=?,grade=? WHERE result_id=".$this->id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrayData);
        Utility::redirect("list_view.php");

    }
    public function trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE student_result SET is_deleted = NOW() WHERE result_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash

    public function trash_view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from student_result WHERE is_deleted <> \'No\' ORDER BY result_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashview();
    public function recover()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE student_result SET is_deleted = 'No' WHERE result_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
    public function delete()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from student_result WHERE result_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash
    public function delete_from_trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from student_result WHERE result_id = ".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from student_result  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();

    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from student_result  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();

    public function search($requestArray){
        $sql = "";

        if(isset($requestArray['byName']))
            $sql = "SELECT * FROM `student_result` WHERE `is_deleted` ='No' AND `result_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `student_result` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->exam_name);
        }

        //$STH = $this->DBH->query($sql);
        // $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        return array_unique($_allKeywords);


    }// get all keywords
}
?>

<?php
namespace App\CollegeClass;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
use PDOException;
class Collegeclass extends DB{
    public $id="";
    public $class_name="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("class_name",$postVariable)){
            $this->class_name=$postVariable['class_name'];
        }

    }

    public function store()
    {
                $arrData = array($this->class_name);
                $sql = "INSERT into class_info(class_name)VALUES (?)";
                $STH = $this->DBH->prepare($sql);
                $result = $STH->execute($arrData);

                if ($result) {
                    Message::message("Success!Data has been inserted Successfully :)");
                } else {
                    Message::message("Falied!Data has not been inserted Successfully :(");
                }

                Utility::redirect('create.php');


    }


    public function index()
    {
            $sql = "SELECT * from class_info WHERE is_deleted='No' ORDER BY class_id DESC";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $arrAllData = $STH->fetchAll();
            return $arrAllData;

    }

    public function view(){
        $sql="SELECT * from class_info where class_id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }


    public function update(){
        $arrData=array($this->class_name);
        $sql="UPDATE class_info SET class_name=? WHERE class_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect("list_view.php");

    }


    public function delete(){
        $sql="DELETE from class_info WHERE class_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");
    }

    public function delete_from_trash(){
        $sql="DELETE from class_info WHERE class_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }




    public function trash(){
        $sql="UPDATE class_info SET is_deleted=NOW() WHERE class_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("list_view.php");

    }

    public function trash_list(){
        $sql="SELECT * from class_info WHERE is_deleted<>'No' ORDER BY class_id DESC";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData=$STH->fetchAll();
    }

    public function recover(){
        $sql="UPDATE class_info SET is_deleted='No' WHERE class_id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect("trash_view.php");
    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from class_info  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from class_info  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }


    public function search($requestArray)
    {
    $sql = "";
    if (isset($requestArray['byClass']) && isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND (class_name LIKE '%" . $requestArray['search'] . "%' OR profile_picture LIKE '%" . $requestArray['search'] . "%')";
    if (isset($requestArray['byClass']) && !isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND class_name LIKE '%" . $requestArray['search'] . "%'";
    if (!isset($requestArray['byClass']) && isset($requestArray['byPicture'])) $sql = "SELECT * FROM class_info WHERE is_deleted ='No' AND class_name LIKE '%" . $requestArray['search'] . "%'";

    $STH = $this->DBH->query($sql);
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $allData = $STH->fetchAll();

    return $allData;
    }

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM class_info WHERE is_deleted ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->class_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->class_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function loadtable()
    {
        $sql = "SELECT * from class_info WHERE is_deleted='No' ORDER BY class_id DESC  ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

    public function className($id){
        $sql="SELECT * from class_info where class_id=$id";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData=$STH->fetch();
        return $arrOneData;
    }


}

?>


<?php
require_once ('../../../vendor/autoload.php');
use App\Student\Student;
use App\Utility\Utility;
//session_start();

$objStudent = new Student();
$objStudent->setData($_GET);
$oneData=$objStudent->view();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>College Management System</title>


    <!-- Bootstrap core CSS -->

    <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../resource/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../../../resource/css/custom.css" rel="stylesheet">
    <link href="../../../resource/css/icheck/flat/green.css" rel="stylesheet">
    <link href="../../../resource/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

    <script src="../../../resource/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="../../../resource/images/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>Anthony Fernando</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <?php require_once ('../sidebar.php');?>
            </div>
        </div>

        <?php require_once ('../top_nav.php');?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            Class
                            <small>
                                Single View
                            </small>
                        </h3>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>View</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>

                            </div>
                            <div class="x_content">

                                <table id="example" class="table table-striped responsive-utilities jambo_table">


                                    <tr><th>Profile Picture</th>
                                        <?php
                                        $path='../../../img/'.$oneData->profile_pic;
                                        ?>
                                        <td><?php echo'<img src="'.$path.'" width="120px" height="100px" align="center">'?></td>

                                    </tr>
                                   <tr><th>ID</th> <td><?php echo $oneData->std_id; ?> </td></tr>
                                    <tr><th>First Name</th> <td><?php echo $oneData->first_name; ?> </td></tr>
                                    <tr><th>Last Name</th> <td><?php echo $oneData->last_name; ?> </td></tr>
                                    <tr><th>Birth Date</th> <td><?php echo $oneData->birth_date; ?> </td></tr>
                                    <tr><th>Father's Name</th> <td><?php echo $oneData->fathers_name; ?> </td></tr>
                                    <tr><th>Father's Profession</th> <td><?php echo $oneData->fathers_profession; ?> </td></tr>
                                    <tr><th>Mother's Name</th> <td><?php echo $oneData->mothers_name; ?> </td></tr>
                                    <tr><th>Mother's Profession</th> <td><?php echo $oneData->mothers_profession; ?> </td></tr>
                                    <tr><th>Nationality</th> <td><?php echo $oneData->nationality; ?> </td></tr>
                                    <tr><th>Gender</th> <td><?php echo $oneData->gender; ?> </td></tr>
                                    <tr><th>Religion</th> <td><?php echo $oneData->religion; ?> </td></tr>
                                    <tr><th>Contact Number</th> <td><?php echo $oneData->contact_no1; ?> </td></tr>
                                    <tr><th>Contact Number</th> <td><?php echo $oneData->contact_no2; ?> </td></tr>
                                    <tr><th>Present Address</th> <td><?php echo $oneData->present_address; ?> </td></tr>
                                    <tr><th>Permanent Address</th> <td><?php echo $oneData->permanent_address; ?> </td></tr>
                                    <tr><th>School Name</th> <td><?php echo $oneData->school_name; ?> </td></tr>
                                    <tr><th>Group</th> <td><?php echo $oneData->ssc_group; ?> </td></tr>
                                    <tr><th>S.S.C Result</th> <td><?php echo $oneData->ssc_result; ?> </td></tr>
                                    <tr><th>S.S.C Roll Number</th> <td><?php echo $oneData->ssc_roll; ?> </td></tr>
                                    <tr><th>S.S.C Registration Number</th> <td><?php echo $oneData->ssc_registration; ?> </td></tr>
                                    <tr><th>Email</th> <td><?php echo $oneData->email; ?> </td></tr>




                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="../../../resource/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="../../../resource/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="../../../resource/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="../../../resource/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="../../../resource/js/icheck/icheck.min.js"></script>

<script src="../../../resource/js/custom.js"></script>


<!-- Datatables -->
<script src="../../../resource/js/datatables/js/jquery.dataTables.js"></script>
<script src="../../../resource/js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
</body>

</html>
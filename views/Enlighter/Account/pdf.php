<?php
include_once ('../../../vendor/autoload.php');
use App\Teacher\Teacher;

$objTeacher=new Teacher();
 $recordSet=$objTeacher->index();
 //var_dump($allData);
$trs="";
$sl=0;




    foreach($recordSet as $row) {
        $id =  $row["teacher_id"];
        $teacher_name = $row['teacher_name'];
        $fathers_name = $row['fathers_name'];
        $mothers_name = $row['mothers_name'];
        $date_of_birth = $row['date_of_birth'];
        $joining_date = $row['joining_date'];
        $nid = $row['nid'];

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='150'> $sl</td>";
        $trs .= "<td width='150'> $id </td>";
        $trs .= "<td width='300'> $teacher_name </td>";
        $trs .= "<td width='300'> $fathers_name </td>";
        $trs .= "<td width='300'> $mothers_name </td>";
        $trs .= "<td width='300'> $date_of_birth </td>";
        $trs .= "<td width='300'> $joining_date </td>";
        $trs .= "<td width='300'> $nid </td>";


        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Name</th>
                    <th align='left' >Fathers Name</th>
                    <th align='left' >Mothers Name</th>
                    <th align='left' >Birth date</th>
                    <th align='left' >Joining date</th>
                    <th align='left' >NID</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');
<?php
require_once("../../../vendor/autoload.php");
use App\Account\Account;
use App\Message\Message;
use App\Utility\Utility;
session_start();

$objAccount = new Account();

$allData = $objAccount->index("obj");
//$serial = 1;

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objAccount->search($_REQUEST);
$availableKeywords=$objAccount->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objAccount->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objAccount->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Atomic Project! | Book Title</title>

    <!-- Bootstrap core CSS -->

    <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../resource/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../../../resource/css/custom.css" rel="stylesheet">
    <link href="../../../resource/css/icheck/flat/green.css" rel="stylesheet">
    <link href="../../../resource/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

    <script src="../../../resource/js/jquery.min.js"></script>
    <script src="../../../resource/js/bootstrap.min.js"></script>


    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
    <!-- required for search, block3 of 5 end -->


</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="../index.php" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="../../../resource/images/user.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>Anthony Fernando</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />
                    <!-- sidebar menu -->
                    <?php require_once ('../sidebar.php');?>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <?php require_once ('../top_nav.php');?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                                Account /
                    <small>
                        List View
                    </small>
                </h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Book List</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right" style="margin-top: 20px">
                                        <form id="searchForm" action="list_view.php"  method="get">
                                            <input type="text" class="form-control" value="" id="searchID" name="search" placeholder="Search" width="60" >

                                            <input type="checkbox"  name="byTitle"   checked  >By Title
                                            <input type="checkbox"  name="byAuthor"  checked >By Author
                                            <input hidden type="submit" class="btn-primary" value="search">
                                        </form>
                                    </div>
                                    
                                </div>
                                <div class="x_content">

                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>Sl. No. </th>
                                                <th>Student Name </th>
                                                <th>Class </th>
                                                <th>Date </th>
                                                <th>Amount </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        foreach($someData as $oneData){

                                        ?>

                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                <td class=" "><?php echo $serial;?></td>
                                                <td class=" "><?php echo $oneData->student_id;?></td>
                                                <td class=" "><?php echo $oneData->class_id;?></td>
                                                <td class=" "><?php echo $oneData->date;?></td>
                                                <td class=" "><?php echo $oneData->amount;?></td>
                                                <td class=" last">
                                                    <a href="view.php?id=<?php echo $oneData->account_id;?>" class="btn btn-success btn-xs">View</a>
                                                    <a href="edit.php?id=<?php echo $oneData->account_id;?>" class="btn btn-primary btn-xs">Edit</a>
                                                    <a href="delete.php?id=<?php echo $oneData->account_id;?>" class="btn btn-danger btn-xs">Delete</a>
                                                    <a href="trash.php?id=<?php echo $oneData->account_id;?>" class="btn btn-warning btn-xs">Trash</a>
                                                    <a href="email.php?id=<?php echo $oneData->account_id;?>" class="btn btn-info btn-xs">Email</a>
                                                </td>
                                            </tr>
                                        <?php
                                            $serial++;
                                        }
                                        ?>
                                        </tbody>

                                    </table>
                                    <div class="clearfix"></div>
                                    <div class="col-md-7 col-sm-7 col-xs-12 pull-right">
                                        <a href="pdf.php" class="btn btn-info btn-group-lg"><i class="fa fa-file-pdf-o"></i> Download as pdf</a>
                                        <a href="xl.php" class="btn btn-success btn-group-lg"><i class="fa fa-file-excel-o"></i> Download as Excel</a>
                                        <a href="email.php?list=1" class="btn btn-warning btn-group-lg"><i class="fa fa-inbox"></i> Email to a Friend</a>

                                    </div>
                                    <div class="clearfix"></div>
                                    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                                    <div align="center" class="container">
                                        <ul class="pagination">

                                            <?php
                                            $pageMinusOne = $page - 1;
                                            $pagePlusOne = $page + 1;
                                            if($page>$pages)
                                            {
                                                Utility::redirect("list_view.php?Page=$pages");
                                            }
                                            if($page>1)
                                            {

                                                echo "<li><a href='?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                                            }
                                            for($i=1;$i<=$pages;$i++)
                                            {
                                                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                                            }
                                            if(!($page == $pages))
                                            {

                                                echo "<li><a href='?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                                            }
                                            ?>
                                            <br>
                                            <br>
                                            <br>


                                            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                                <?php
                                                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                                                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                                                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                                                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                                                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                                                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                                                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                                                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                                                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                                                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                                                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                                                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                                                ?>
                                            </select>
                                        </ul>
                                    </div>
                                    <!--  ######################## pagination code block#2 of 2 end ###################################### -->


                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
                </div>

                </div>
                <!-- /page content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <script src="../../../resource/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="../../../resource/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="../../../resource/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="../../../resource/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="../../../resource/js/icheck/icheck.min.js"></script>

        <script src="../../../resource/js/custom.js"></script>


        <!-- Datatables -->
        <script src="../../../resource/js/datatables/js/jquery.dataTables.js"></script>
        <script src="../../../resource/js/datatables/tools/js/dataTables.tableTools.js"></script>
        <script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>

    <script src="../../../resource/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/js/jquery-ui.js"></script>

    <!-- required for search, block 5 of 5 start -->
    <script>

        $(function() {
            var availableTags = [

                <?php
                echo $comma_separated_keywords;
                ?>
            ];
            // Filter function to search only from the beginning of the string
            $( "#searchID" ).autocomplete({
                source: function(request, response) {

                    var results = $.ui.autocomplete.filter(availableTags, request.term);

                    results = $.map(availableTags, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });

                    response(results.slice(0, 15));

                }
            });


            $( "#searchID" ).autocomplete({
                select: function(event, ui) {
                    $("#searchID").val(ui.item.label);
                    $("#searchForm").submit();
                }
            });


        });

    </script>
    <!-- required for search, block5 of 5 end -->
</body>


</html>
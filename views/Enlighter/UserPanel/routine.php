<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('/college_management_system/index.php');
    return;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>College Management System</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/resource2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>


    <!-- Fonts -->
    <link href="../../../resource/resource2/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../../../resource/resource2/css/animate.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="../../../resource/resource2/css/style.css" rel="stylesheet">
    <link href="../../../resource/resource2/color/default.css" rel="stylesheet">

    <link href="../../../resource/resource2/css/mystyle.css" rel="stylesheet">


</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" style="background-color: #bbc8ff">
<!-- Preloader -->
<div id="preloader" style="background-color: #bbc8ff">
    <div id="load"></div>
</div>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="background-color: #bbc8ff">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="index.php">

            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse" >
            <ul class="nav navbar-nav" style="background-color: #bbc8ff">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="index.php">About</a></li>
                <li><a href="index.php">Gallery</a></li>
                <li><a href="admission_form.php">Admission Form</a></li>
                <li><a href="Authentication/logout.php">Logout</a></li>
                <li><a href="index.php">Contact</a></li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>    <!-- /.container -->



<script src="../../../resource/resource2/js/jquery.min.js"></script>
<script src="../../../resource/resource2/js/bootstrap.min.js"></script>
<script src="../../../resource/resource2/js/jquery.easing.min.js"></script>
<script src="../../../resource/resource2/js/jquery.scrollTo.js"></script>
<script src="../../../resource/resource2/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="../../../resource/resource2/js/custom.js"></script>
<script src="../../../contactform/contactform.js"></script>


</body>

</html>
<script>

    $(function() {

        $('#login-form-link').click(function(e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function(e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

</script>


<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
        <h3>College Manegent System</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/User/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/User/index.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/User/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i> Teacher <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Teacher/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Teacher/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Teacher/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i> Student <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Student/admission_form.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Student/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Student/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-group"></i> Group <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Group/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Group/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Group/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-child"></i> Class <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/CollegeClass/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/CollegeClass/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/CollegeClass/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <!--li><a><i class="fa fa-child"></i> Section <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Section/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Section/index.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Section/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li-->
            <!--li><a><i class="fa fa-child"></i> Routine <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Routine/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Routine/index.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Routine/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li-->
            <li><a><i class="fa fa-comments"></i>Attendance <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Attendance/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Attendance/index.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Attendance/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-comments"></i>Exam <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Exam/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Exam/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Exam/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-comments"></i>Result <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Result/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Result/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Result/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-comments"></i>Subject <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Subject/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Subject/list_view.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Subject/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-comments"></i>Account <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/College_Management_System/views/Enlighter/Accounce/create.php"> Add New </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Accounce/index.php"> List View </a>
                    </li>
                    <li><a href="/College_Management_System/views/Enlighter/Accounce/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->
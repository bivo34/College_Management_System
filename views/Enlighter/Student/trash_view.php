<?php
require_once ('../../../vendor/autoload.php');
use App\Student\Student;
use App\Utility\Utility;
$objStudent = new Student();
$allData = $objStudent->trash_list();



######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objStudent->trashedPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>College Management System</title>
    <script language="JavaScript" type="text/javascript">
        function ConfirmDelete() {
            return confirm("Are you sure you want to delete?");
        }


    </script>

    <!-- Bootstrap core CSS -->

    <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../resource/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../../../resource/css/custom.css" rel="stylesheet">
    <link href="../../../resource/css/icheck/flat/green.css" rel="stylesheet">
    <link href="../../../resource/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

    <script src="../../../resource/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="../../../resource/images/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>Anthony Fernando</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <?php require_once ('../sidebar.php');?>
            </div>
        </div>

        <?php require_once ('../top_nav.php');?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            Student
                            <small>
                                Trash View
                            </small>
                        </h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Trash List</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <table id="example" class="table table-striped responsive-utilities jambo_table">
                                    <thead>
                                    <tr class="headings">
                                        <th>
                                            <input type="checkbox" class="tableflat">
                                        </th>
                                        <th>Sl.No.</th>
                                        <th>ID</th>
                                        <th>Full Name </th>
                                        <th>Result</th>
                                        <th>Roll Number</th>
                                        <th>Registration Number</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php

                                    foreach($someData as $oneData)
                                    {

                                        ?>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <input type="checkbox" class="tableflat">
                                            </td>
                                            <td><?php echo $serial; ?></td>
                                            <td><?php echo $oneData->std_id; ?> </td>
                                            <td><?php echo $oneData->first_name; ?></td>
                                            <td><?php echo $oneData->ssc_result; ?> </td>
                                            <td><?php echo $oneData->ssc_roll; ?> </td>
                                            <td><?php echo $oneData->ssc_registration; ?> </td>
                                            <td>
                                                <a href="recover.php?id=<?php echo $oneData->std_id;?>" data-placement="top" data-toggle="tooltip" title="Recover"><button class="btn btn-warning btn-xs" data-title="Recover" data-toggle="modal">
                                                        <span class="glyphicon glyphicon-ok-sign"></span></button></a>
                                                <a href="delete_from_trash.php?id=<?php echo $oneData->std_id;?>" data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" onclick="return ConfirmDelete()" data-title="Delete" data-toggle="modal">
                                                        <span class="glyphicon glyphicon-remove"></span></button></a>

                                            </td>
                                        </tr>
                                        <?php
                                        $serial++;
                                    }
                                    ?>

                                    </tbody>

                                </table>




                                <!--  ######################## pagination code block#2 of 2 start ###################################### -->

                                <div align="center" class="container">
                                    <ul class="pagination">

                                        <?php

                                        $pageMinusOne  = $page-1;
                                        $pagePlusOne  = $page+1;
                                        if($page>$pages) Utility::redirect("trash_view.php?Page=$pages");

                                        if($page>1)  echo "<li><a href='trash_view.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                                        for($i=1;$i<=$pages;$i++)
                                        {
                                            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                                        }
                                        if($page<$pages) echo "<li><a href='trash_view.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                                        ?>


                                    </ul>

                                    <div class="col-sm-5">

                                        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                            <?php
                                            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                                            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                                            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                                            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                                            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                                            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                                            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                                            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                                            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                                            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                                            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                                            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                                            ?>
                                        </select>
                                    </div>


                                </div>

                                <!--  ######################## pagination code block#2 of 2 end ###################################### -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="../../../resource/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="../../../resource/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="../../../resource/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="../../../resource/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="../../../resource/js/icheck/icheck.min.js"></script>

<script src="../../../resource/js/custom.js"></script>


<!-- Datatables -->
<script src="../../../resource/js/datatables/js/jquery.dataTables.js"></script>
<script src="../../../resource/js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
</body>

</html>
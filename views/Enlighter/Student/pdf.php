<?php
include_once ('../../../vendor/autoload.php');
use App\Student\Student;

$objStudent= new Student();
$recordSet=$objStudent->index();
 //var_dump($allData);
$trs="";
$sl=0;




    foreach($recordSet as $row) {
        $id =  $row->std_id;
        //$firstname = $row->first_name;
        //$secondname=$row->last_name;
        $fullname=($row->first_name)." ".($row->last_name);
        $birthdate=$row->birth_date;
        $fathername=$row->fathers_name;
        $mothername=$row->mothers_name;
        $nationality=$row->nationality;
        $gender=$row->gender;
        $religion=$row->religion;
        $contact=$row->contact_no1;
        $address=$row->permanent_address;
        $sscresult=$row->ssc_result;
        $sscroll=$row->ssc_roll;
        $sscregis=$row->ssc_registration;
        $email=$row->email;
        $picture = $row->profile_pic;


        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='150'> $sl</td>";
        $trs .= "<td width='150'> $id </td>";
        $trs .= "<td width='150'> $fullname </td>";
       // $trs .= "<td width='150'> $secondname </td>";
        $trs .= "<td width='150'> $birthdate </td>";
        $trs .= "<td width='150'> $fathername </td>";
        $trs .= "<td width='150'> $mothername </td>";
        $trs .= "<td width='150'> $nationality </td>";
        $trs .= "<td width='150'> $gender </td>";
        $trs .= "<td width='150'> $religion </td>";
        $trs .= "<td width='150'> $contact </td>";
        $trs .= "<td width='150'> $address </td>";
        $trs .= "<td width='150'> $sscresult </td>";
        $trs .= "<td width='150'> $sscroll </td>";
        $trs .= "<td width='150'> $sscregis </td>";
        $trs .= "<td width='150'> $email </td>";
        $trs .= "<td width='300'> <img src='../../../img/$picture'> </td>";


        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Full Name</th>
                    <th align='left' >Birth date</th>
                    <th align='left' >Father's Name</th>
                    <th align='left' >Mother's Name</th>
                    <th align='left' >Nationality</th>
                    <th align='left' >Gender</th>
                    <th align='left' >Religion</th>
                    <th align='left' >Contact</th>
                    <th align='left' >Address</th>
                    <th align='left' >SSC Result</th>
                    <th align='left' >SSC Roll</th>
                    <th align='left' >SSC Registration</th>
                    <th align='left' >Email</th>
                    <th align='left' >Profile Picture</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');
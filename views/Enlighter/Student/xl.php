<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

require_once("../../../vendor/autoload.php");
use App\Student\Student;
$objStudent= new Student();

$allData = $objStudent->index();

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Umme Batul Rumpa")
    ->setLastModifiedBy("Umme Batul Rumpa")
    ->setTitle("PHPExcel Test Document")
    ->setSubject("PHPExcel Test Document")
    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
    ->setKeywords("office PHPExcel php")
    ->setCategory("Test result file");


// Add some data

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Serial')
    ->setCellValue('B1', 'ID')
    ->setCellValue('C1', 'First Name')
    ->setCellValue('D1', 'Last Name')
    ->setCellValue('E1', 'Birth Date')
    ->setCellValue('F1', 'Father\'s  Name')
    ->setCellValue('G1', 'Father\'s Profession')
    ->setCellValue('H1', 'Mother\'s  Name')
    ->setCellValue('I1', 'Mother\'s  Profession')
    ->setCellValue('J1', 'Nationality')
    ->setCellValue('K1', 'Gender')
    ->setCellValue('L1', 'Religion')
    ->setCellValue('M1', 'Contact No 1')
    ->setCellValue('N1', 'Contact No 2')
    ->setCellValue('O1', 'Present Address')
    ->setCellValue('P1', 'Permanent Address')
    ->setCellValue('Q1', 'School Name')
    ->setCellValue('R1', 'SSC Group')
    ->setCellValue('S1', 'SSC Result')
    ->setCellValue('T1', 'SSC Roll No')
    ->setCellValue('U1', 'SSC Registration No')
    ->setCellValue('V1', 'Email');


$sl=0;
$counter=1;

foreach ($allData as $oneData) {
    $sl++;
    $counter++;
// Miscellaneous glyphs, UTF-8
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $counter, $sl)
        ->setCellValue('B' . $counter, $oneData->std_id)
        ->setCellValue('C' . $counter, $oneData->first_name)
        ->setCellValue('D' . $counter, $oneData->last_name)
        ->setCellValue('E' . $counter, $oneData->birth_date)
        ->setCellValue('F' . $counter, $oneData->fathers_name)
        ->setCellValue('G' . $counter, $oneData->fathers_profession)
        ->setCellValue('H' . $counter, $oneData->mothers_name)
        ->setCellValue('I' . $counter, $oneData->mothers_profession)
        ->setCellValue('J' . $counter, $oneData->nationality)
        ->setCellValue('K' . $counter, $oneData->gender)
         ->setCellValue('L' . $counter, $oneData->religion)
         ->setCellValue('M' . $counter, $oneData->contact_no1)
        ->setCellValue('N' . $counter, $oneData->contact_no2)
         ->setCellValue('O' . $counter, $oneData->present_address)
          ->setCellValue('P' . $counter, $oneData->permanent_address)
         ->setCellValue('Q' . $counter, $oneData->school_name)
          ->setCellValue('R' . $counter, $oneData->ssc_group)
        ->setCellValue('S' . $counter, $oneData->ssc_result)
        ->setCellValue('T' . $counter, $oneData->ssc_roll)
          ->setCellValue('U' . $counter, $oneData->ssc_registration)
        ->setCellValue('V' . $counter, $oneData->email);

}
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Student Details Sheet');

$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="userList.xlsx"');

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="List.xlsx"');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

ob_end_clean();


$objWriter->save('php://output');
exit;

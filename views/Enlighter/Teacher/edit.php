<?php


require_once("../../../vendor/autoload.php");

use App\Message\Message;
use App\Teacher\Teacher;

$objTeacher = new Teacher();
$objTeacher->setData($_GET);
$oneData= $objTeacher->view("obj");

//echo Message::getMessage();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentallela Alela! | </title>

    <!-- Bootstrap core CSS -->

    <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../resource/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../../../resource/css/custom.css" rel="stylesheet">
    <link href="../../../resource/css/icheck/flat/green.css" rel="stylesheet">


    <script src="../../../resource/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="../index.php" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                </div>
                <div class="clearfix"></div>


                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="../../../resource/images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>Anthony Fernando</h2>
                    </div>
                </div>
                <br />
                <?php
                require_once('../sidebar.php');
                ?>
                <!-- /menu prile quick info -->

            </div>
        </div>

        <!-- top navigation -->
        <?php require_once('../top_nav.php');?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>
                            Book Title / Edit
                        </h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <form class="form-horizontal form-label-left" novalidate method="post" action="update.php" enctype="multipart/form-data">

                                    <span class="section">Edit Book Title</span>
                                    <?php
                                    // echo Message::message();
                                    ?>
                                    <div class="item form-group">
                                        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject ID <span class="required">*</span>
                                        </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="subject_id[]">
                                            <option value="Choose" <?php if ($oneData->subject_id=='Choose..') { echo "SELECTED"; } ?>>Choose</option>
                                            <option value="1" <?php if ($oneData->subject_id=='1') { echo "SELECTED"; } ?>>Choose</option>
                                            <option value="2" <?php if ($oneData->subject_id=='2') { echo "SELECTED"; } ?>>Bangla</option>
                                            <option value="3" <?php if ($oneData->subject_id=='3') { echo "SELECTED"; } ?>>English</option>
                                            <option value="4" <?php if ($oneData->subject_id=='4') { echo "SELECTED"; } ?>>Accounting</option>
                                            <option value="5" <?php if ($oneData->subject_id=='5') { echo "SELECTED"; } ?>>Other</option>
                                        </select>
                                    </div>
                                        </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Teacher's name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="1" name="teacher_name" value="<?php echo $oneData->teacher_name;?>" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="father name"> Father's name
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="father_name" data-validate-length-range="5,20" value="<?php echo $oneData->father_name;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mother name"> Mother's name
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="mother_name" data-validate-length-range="5,20" value="<?php echo $oneData->mother_name;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="spouse name"> Spouse name
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="spouse_name" data-validate-length-range="5,20" value="<?php echo $oneData->spouse_name;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Birth date"> Birth date
                                        </label>
                                        <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="single_cal1" name="birth_date" value="<?php echo $oneData->birth_date;?>" aria-describedby="inputSuccess2Status">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Joining date"> Joining date
                                        </label>
                                        <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="single_cal4" name="joining_date" value="<?php echo $oneData->joining_date;?>" aria-describedby="inputSuccess2Status4">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality"> Nationality
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="nationality" data-validate-length-range="5,20" value="<?php echo $oneData->nationality;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="religion"> Religion
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="religion[]">
                                                <option value="Buddhist" <?php if ($oneData->religion=='Buddhist') { echo "SELECTED"; } ?>>Buddhist</option>
                                                <option value="Christian" <?php if ($oneData->religion=='Christian') { echo "SELECTED"; } ?>>Christian</option>
                                                <option value="Hindu" <?php if ($oneData->religion=='Hindu') { echo "SELECTED"; } ?>>Hindu</option>
                                                <option value="Islam" <?php if ($oneData->religion=='Islam') { echo "SELECTED"; } ?>>Islam</option>
                                                <option value="Other" <?php if ($oneData->religion=='Other') { echo "SELECTED"; } ?>>Other</option>
                                            </select>
                                        </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="NID"> National ID
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="NID" data-validate-length-range="5,20" value="<?php echo $oneData->nid;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact number"> Contact number
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="occupation" type="text" name="contact_number" data-validate-length-range="5,20" value="<?php echo $oneData->contact_number;?>" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Present Address"> Present Address</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="message" required="required" class="form-control" name="present_address" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"><?php echo $oneData->present_address;?></textarea>

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Permanent Address"> Permanent Address</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="message" required="required" class="form-control" name="permanent_address" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"><?php echo $oneData->permanent_address;?></textarea>

                                        </div>
                                    </div>
                                    <div class="item form-group">

                                        <?php
                                        $path='../../../TeachersPhoto/'.$oneData->photo;
                                        ?>
                                        <?php echo'<img src="'.$path.'" style="width:120px;height:100px;margin-left:270px"; >'?>

                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Profile Picture <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" name="profile_photo" >
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="reset" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="../../../resource/js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="../../../resource/js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="../../../resource/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="../../../resource/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="../../../resource/js/icheck/icheck.min.js"></script>

<script src="../../../resource/js/custom.js"></script>
<!-- form validation -->
<script src="../../../resource/js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>

</body>

</html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>prop demo</title>
    <style>
        img {
            padding: 10px;
        }
        div {
            color: red;
            font-size: 24px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<form class="form">
    <input type="checkbox" class="subject1">
    <input type="checkbox" class="subject">
    <input type="checkbox" class="subject">
    <input type="checkbox" class="subject">

</form>

<script>
    $( "input[type='checkbox'].subject" ).prop({
        disabled: true
    });
    if ( $( "input[type='checkbox'].subject" ).prop( "checked" ) ){
alert();
    }
</script>

</body>
</html>
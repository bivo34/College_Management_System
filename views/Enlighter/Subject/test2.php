<!DOCTYPE html>
<html>
<body>

<div id="demo">

    <form class="form">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Subject category"> Subject category </label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="flat subject" id="common" name="subject_category[]" value="Common subject" onclick="loadDoc()"> Common subject
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="flat subject" id="optional" name="subject_category[]" value="Optional subject"> Optional subject
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="flat subject" id="group" name="subject_category[]" value="Group subject"> Group subject
                </label>
            </div>
        </div>
    </form>
</div>

<script>
    function loadDoc() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML =
                    this.responseText;
            }
        };
        xhttp.open("GET", "ajax_info.txt", true);
        xhttp.send();
    }
</script>

<script>
    $('input').attr('disabled',true);

</script>

</body>
</html>

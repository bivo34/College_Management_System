<?php

require_once("../../../vendor/autoload.php");
use App\Teacher\Teacher;

$objTeacher = new Teacher();
//$_FILES
if($_FILES['profile_photo']['name'])
{
    $file_name = time().$_FILES['profile_photo']['name'];
    $temporary_location = $_FILES['profile_photo']['tmp_name'];

    move_uploaded_file($temporary_location,'../../../TeachersPhoto/'.$file_name);

    $_POST['profile_photo']=$file_name;
}
$objTeacher->setData($_POST);
$objTeacher->update();
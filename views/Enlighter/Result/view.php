<?php

require_once("../../../vendor/autoload.php");

use App\Result\Result;
use App\Student\Student;
use App\Exam\Exam;
use App\Subject\Subject;
use App\CollegeClass\Collegeclass;

$objResult  =  new Result();
$objResult->setData($_GET);
$oneData= $objResult->view();

$objst = new Student();
$student=$objst->loadStudentName($oneData->student_id);
$objExam = new Exam();
$exam=$objExam->examName($oneData->exam_id);
$objSub = new Subject();
$sub=$objSub->loadSubjectName($oneData->subject_id);

$objClass = new Collegeclass();
$class=$objClass->className($oneData->class_id);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>College Management System! | Result</title>

    <!-- Bootstrap core CSS -->

    <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../resource/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../../../resource/css/custom.css" rel="stylesheet">
    <link href="../../../resource/css/icheck/flat/green.css" rel="stylesheet">


    <script src="../../../resource/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="../index.php" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="../../../resource/images/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>Admin</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <?php require_once ('../sidebar.php');?>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
                <?php require_once ('../top_nav.php');?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                    Details

                </h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Result Details </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-list"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Trash List</a>
                                                </li>
                                                <li><a href="#">List View</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <section class="content invoice">

                                        <div class="row invoice-info">
                                            <div class="col-sm-4 invoice-col">
                                                ID # <?php  echo $oneData->result_id; ?>
                                                <br>
                                                <strong>Student Name : <?php echo $student->first_name . $student->last_name;?></strong>
                                                <br>Exam name : <?php echo $exam->exam_name; ?>
                                                    <br>Class name : <?php echo $class->class_name;?>
                                                    <br>Subject name : <?php echo $sub->subject_name;?>
                                                <br>Marks : <?php echo $oneData->marks;?>
                                                <br>Grade point : <?php echo $oneData->grade_point;?>
                                                <br>Grade : <?php echo $oneData->grade;?>

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="../../../resource/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="../../../resource/js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="../../../resource/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="../../../resource/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="../../../resource/js/icheck/icheck.min.js"></script>

    <script src="../../../resource/js/custom.js"></script>

</body>

</html>
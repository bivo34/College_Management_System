<?php
include_once('vendor/autoload.php');
if(!isset($_SESSION) )session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>College Management System</title>

    <!-- Bootstrap Core CSS -->
    <link href="resource/resource2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="resource/bootstrap/css/bootstrap.min.css">
    <script src="resource/bootstrap/js/jquery.min.js"></script>
    <script src="resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>


    <!-- Fonts -->
    <link href="resource/resource2/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="resource/resource2/css/animate.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="resource/resource2/css/style.css" rel="stylesheet">
    <link href="resource/resource2/color/default.css" rel="stylesheet">

    <link href="resource/resource2/css/mystyle.css" rel="stylesheet">


</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" style="background-color: #bbc8ff">
<!-- Preloader -->
<div id="preloader" style="background-color: #bbc8ff">
    <div id="load"></div>
</div>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="background-color: #bbc8ff">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="index.php"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse" >
            <ul class="nav navbar-nav" style="background-color: #bbc8ff">
                <li class="active"><a href="#intro">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#service">Gallery</a></li>
                <li><a href="views/Enlighter/UserPanel/admission_form.php">Admission Form</a></li>
                <li><a href="#login">Login</a></li>
                <li><a href="#contact">Contact</a></li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>    <!-- /.container -->


<!-- Section: intro -->
<section id="intro" class="intro">

    <div class="slogan">
        <h2>WELCOME TO <span class="text_color">OUR COLLEGE</span> </h2>
        <h5 style="color:white">Learn as  much as you can while you are young,since life becomes too busy later.</h5>
    </div>
    <div class="page-scroll">
        <a href="#service" class="btn btn-circle">
            <i class="fa fa-angle-double-down animated"></i>
        </a>
    </div>
</section>

<!-- /Section: intro -->

<section id="about" class="home-section text-center" style="background-color: #fff7ff" >
    <div class="heading-about" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>About us</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="para">

                <p style="font-family:Arial">
                    The organization was born in 1836 in Chittagong Zila School.
                    Thirty-three years after the establishment in 1869, it was promoted to high school.
                    Since then it is known as Chittagong Government College Chittagong College.
                    Cakabajarera College Road, one side of the parade ground of the Chittagong angle of a Portuguese-era establishment of the college started functioning.
                    After college, promoted by Mr. JC Bose was appointed as the first principal.
                    Since 1909, the Department of the College of Arts Science was introduced as well as the higher secondary level.
                    In 1910, the college's first class degree from the University of Calcutta College was recognized.
                    Accordingly, from the College of Mathematics, Chemistry and Physics graduate and a BA (Hons) starts teaching about the issues.
                    Since graduating class of 1919, class is in English and philosophy and economics supplement is added.
                    In 1924 the college became the first principal of Kamaluddin Ahmed Shamsul Ulema.
                    His progress was rapid during college.Sher Bengali AK Fazlul Huq this time laid the foundation stone of the hostel.
                    Shamsul Ulemas practiced at the college began to publish the magazine.
                    The most significant event of his being the introduction of co-education. </p>
            </div>


        </div>
    </div>
</section>
<!-- /Section: about -->


<!-- Section: services -->

<section id="service" class="home-section text-center bg-gray" style="background-color: #bbc8ff" >

    <div class="heading-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Our Gallery</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
                <li data-target="#myCarousel" data-slide-to="5"></li>
                <li data-target="#myCarousel" data-slide-to="6"></li>
                <li data-target="#myCarousel" data-slide-to="7"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="resource/resource2/img/6.jpg" alt="Chania" style="...">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/8.jpg" alt="Chania" style="width:750px;height: 345px;">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/10.jpg" alt="Flower" style="width:750px;height: 345px;">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/14.jpg" alt="Flower" style="width:750px;height: 345px;">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/2.jpg" alt="Flower" style="width:750px;height: 345px;">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/13.JPG" alt="Flower" style="width:750px;height: 345px;">
                </div>

                <div class="item">
                    <img src="resource/resource2/img/12.jpg" alt="Flower" style="width:750px;height: 345px;">
                </div>



            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

</section>
<!-- /Section: services -->
<!-- Section: contact -->
<section id="login" class="home-section text-center" style="background-color: whitesmoke">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Log In Me!!</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading" style="background-color: #bbc8ff">
                        <div class="row" >
                            <div class="col-xs-6">
                                <a href="#" class="active" id="login-form-link" style="padding-left: 95%">Login</a>
                            </div>

                            <div class="col-xs-6">
                                <a href="#" id="register-form-link"></a>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="panel-body" style="background-color: #bbc8ff">
                        <div class="row" >
                            <div class="col-lg-12" >
                                <form id="login-form" action="/college_management_system/views/Enlighter/UserPanel/Authentication/login.php" method="post" role="form" >
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group text-center">
                                        <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                        <label for="remember"> Remember Me</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="/college_management_system/views/Enlighter/UserPanel/forgotten.php" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--form id="register-form" action="http://phpoll.com/register/process" method="post" role="form" style="display: none;">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                                            </div>
                                        </div>
                                    </div>
                                </form-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /Section: contact -->


<!-- Section: contact -->
<section id="contact" class="home-section text-center" style="margin-bottom: 30%">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Get in touch</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="boxed-grey">

                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>

                    <form id="contact-form" action="" method="post" role="form" class="contactForm">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">
                                        Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <label for="email">
                                        Email Address</label>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subject">
                                        Subject</label>
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">
                                        Message</label>
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-skin pull-right" id="btnContactUs">
                                    Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="widget-contact">
                    <h5>Main Office</h5>

                    <address>
                        <strong>Enlighter</strong><br>
                        Tower 795 Folsom Ave, Beautiful Suite 600<br>
                        Chittagong, CA 94107<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890
                    </address>

                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:#">sq_qurashi@yahoo.com</a>
                    </address>


                </div>
            </div>
        </div>

    </div>
</section>

<script src="resource/resource2/js/jquery.min.js"></script>
<script src="resource/resource2/js/bootstrap.min.js"></script>
<script src="resource/resource2/js/jquery.easing.min.js"></script>
<script src="resource/resource2/js/jquery.scrollTo.js"></script>
<script src="resource/resource2/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="resource/resource2/js/custom.js"></script>
<script src="contactform/contactform.js"></script>


</body>

</html>
<script>

    $(function() {

        $('#login-form-link').click(function(e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function(e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

</script>
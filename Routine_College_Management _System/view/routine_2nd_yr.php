<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->

    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../bootstrap/fonts/css/font-awesome.min.css" rel="stylesheet">


    <!-- Custom styling plus plugins -->
    <link href="../bootstrap/css/custom.css" rel="stylesheet">
    <link href="../bootstrap/css/icheck/flat/green.css" rel="stylesheet">
    <link href="../bootstrap/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">



<body style="background-color: azure">

<div class="x_content">

    <table id="example" class="table table-striped responsive-utilities jambo_table">
        <thead>
        <tr class="headings">

            <h1 style="text-align: center">Class Routine</h1>
            <h1 style="text-align: center"> 2nd Year</h1>
            <h2 style="text-align: center" >Science Group</h2>
            <th>Day/Time</th>
            <th>9.00 - 9.45</th>
            <th> 9.45-10.30</th>
            <th>10.30-11.15</th>
            <th>11.15-12.00</th>

            <th>Break</th>
            <th>1.30-2.15</th>
            <th>2.15-3.00</th>
            <th>3.00-3.45</th>
            <th>3.45-4.30</th>
        </tr>
        </thead>
        <tbody  id="example" class="table table-striped responsive-utilities jambo_table">
        <tr  class="even pointer">



            <td>Saturday</td>
            <td>Physics</td>
            <td>Chemistry</td>
            <td> Botany </td>
            <td> Math</td>
            <td> ***</td>
            <td> Math Practical</td>
            <td > Ict</td>
            <td> ***</td>
            <td> Bengali</td>
        </tr>
        <tr class="even pointer">

            <td >Sunday</td>
            <td>"</td>
            <td> Math</td>
            <td> Botany Practical </td>
            <td> Botany Practical</td>
            <td> ***</td>
            <td> Chemistry</td>
            <td> Bengali </td>
            <td> ***</td>
            <td> English</td>


        </tr>

        <tr class="even pointer">

            <td> Monday</td>
            <td>Ict</td>
            <td> English</td>
            <td> Bengali </td>
            <td>Chemistry</td>
            <td> ***</td>
            <td> Math</td>
            <td> Physics Practical</td>
            <td> ***</td>
            <td> Botany</td>


        </tr>
        </tr>
        <tr class="even pointer">

            <td >Tuesday</td>
            <td>"</td>
            <td> Botany</td>
            <td> English </td>
            <td> Bengali</td>
            <td> ***</td>
            <td> ***</td>
            <td> "</td>
            <td> ***</td>
            <td> Math</td>

        </tr>

        <tr class="even pointer">

            <td >Wednesday</td>
            <td>Math</td>
            <td> Bengali</td>
            <td> English </td>
            <td> Chemistry Practical</td>
            <td> ***</td>
            <td> ***</td>
            <td> Physics </td>
            <td> Zoology</td>
            <td> ***</td>


        </tr>

        <tr class="even pointer">

            <td >Thursday</td>
            <td>Chemistry</td>
            <td> English</td>
            <td> Bengali</td>
            <td> Zoology Practical</td>
            <td> ***</td>
            <td> Ict</td>
            <td> Zoology</td>
            <td> ***</td>
            <td> Botany</td>





        </tbody>
    </table>
    <div class="clearfix"></div>
    <div class="col-md-7 col-sm-7 col-xs-12 pull-right">
        <a href="pdf.php" class="btn btn-info btn-group-lg"><i class="fa fa-file-pdf-o"></i> Download as pdf</a>
        <a href="email.php?list=1" class="btn btn-warning btn-group-lg"><i class="fa fa-inbox"></i> Send As Email </a>

    </div>


    <div class="x_content">

        <table id="example" class="table table-striped responsive-utilities jambo_table">
            <thead>
            <tr class="headings">
                <h2 style="text-align: center" >Commerce Group</h2>
                <th>Day/Time</th>
                <th>9.00 - 9.45</th>
                <th> 9.45-10.30</th>
                <th>10.30-11.15</th>
                <th>11.15-12.00</th>

                <th>Break</th>
                <th>1.30-2.15</th>
                <th>2.15-3.00</th>
                <th>3.00-3.45</th>
                <th>3.45-4.30</th>
            </tr>
            </thead>
            <tbody  id="example" class="table table-striped responsive-utilities jambo_table">
            <tr class="even pointer">

                <td >Saturday</td>
                <td>Business Entrepreneurship</td>
                <td> Finance Banking & Bima</td>
                <td> Bengali </td>
                <td> Ict</td>
                <td> ***</td>
                <td> ***</td>
                <td>  English</td>
                <td> ***</td>
                <td> Production Management & Marketing </td>


            </tr>
            <tr class="even pointer">

                <td >Sunday</td>
                <td>Ict</td>
                <td>  English</td>
                <td> Production Management & Marketing</td>
                <td>Finance Banking & Bima  </td>
                <td> ***</td>
                <td> ***</td>
                <td> Accounting </td>
                <td> ***</td>
                <td> Bengali</td>


            </tr>
            <tr class="even pointer">

                <td >Monday</td>
                <td>Business Organization & Management</td>
                <td> ***</td>
                <td> Bengali </td>
                <td>Production Management & Marketing</td>
                <td> ***</td>
                <td> ***</td>
                <td> ***</td>
                <td> Business Entrepreneurship</td>
                <td> Accounting</td>


            </tr>
            <tr class="even pointer">

                <td >Tuesday</td>
                <td>"</td>
                <td>Finance Banking & Insurance</td>
                <td> Accounting </td>
                <td> " </td>
                <td> ***</td>
                <td>  English</td>
                <td> Business Organization & Management</td>
                <td> ***</td>
                <td> Bengali</td>


            </tr>
            <tr class="even pointer">

                <td >Wednesday</td>
                <td>Accounting</td>
                <td> Business Organization & Management</td>
                <td>Ict </td>
                <td> Bengali</td>
                <td> ***</td>
                <td> ***</td>
                <td>  English</td>
                <td> ***</td>
                <td>Business Entrepreneurship</td>


            </tr>


            <tr class="even pointer">



                <td>Thursday</td>
                <td>Bengali</td>
                <td>  English</td>
                <td> Accounting</td>
                <td> Ict</td>
                <td> ***</td>
                <td> Finance Banking & Bima</td>
                <td > ***</td>
                <td> ***</td>
                <td> ***</td>
            </tr>




            <tbody>
        </table>
        <div class="clearfix"></div>
        <div class="col-md-7 col-sm-7 col-xs-12 pull-right">
            <a href="pdf.php" class="btn btn-info btn-group-lg"><i class="fa fa-file-pdf-o"></i> Download as pdf</a>
            <a href="email.php?list=1" class="btn btn-warning btn-group-lg"><i class="fa fa-inbox"></i> Send As Email </a>

        </div>

        <div class="x_content">

            <table id="example" class="table table-striped responsive-utilities jambo_table">
                <thead>
                <tr class="headings">
                    <h2 style="text-align: center" > Humanities Group</h2>
                    <th>Day/Time</th>
                    <th>9.00 - 9.45</th>
                    <th> 9.45-10.30</th>
                    <th>10.30-11.15</th>
                    <th>11.15-12.00</th>

                    <th>Break</th>
                    <th>1.30-2.15</th>
                    <th>2.15-3.00</th>
                    <th>3.00-3.45</th>
                    <th>3.45-4.30</th>
                </tr>
                </thead>
                <tbody  id="example" class="table table-striped responsive-utilities jambo_table">
                <tr >



                    <td>Saturday</td>
                    <td> English</td>
                    <td> Civics & Good Governance</td>
                    <td> Bengali</td>
                    <td> ***</td>
                    <td> ***</td>
                    <td> Ict</td>
                    <td > Sociology</td>
                    <td> ***</td>
                    <td>  Economics</td>
                </tr>
                <tr class="even pointer">

                    <td > Sunday</td>
                    <td> Civics & Good Governance</td>
                    <td> Sociology</td>
                    <td> Civics </td>
                    <td>  Economics</td>
                    <td> ***</td>
                    <td> ***</td>
                    <td> Bengali</td>
                    <td> ***</td>
                    <td> English</td>


                </tr>
                <tr class="even pointer">

                    <td >Monday</td>
                    <td>Ict</td>
                    <td> Civics & Good Governance</td>
                    <td> Sociology </td>
                    <td> Civics</td>
                    <td> ***</td>
                    <td> ***</td>
                    <td> English</td>
                    <td>  Logic</td>
                    <td> ***</td>


                </tr>
                <tr class="even pointer">

                    <td >Tuesday</td>
                    <td> "</td>
                    <td> Logic</td>
                    <td> Civics </td>
                    <td> Economics</td>
                    <td> ***</td>
                    <td> Social Work</td>
                    <td> Bengali </td>
                    <td> ***</td>
                    <td> English</td>


                </tr>

                <tr class="even pointer">

                    <td >Wednesday</td>
                    <td>Social Work</td>
                    <td> Civics & Good Governance</td>
                    <td> English </td>
                    <td>  "</td>
                    <td> ***</td>
                    <td>Bengali</td>
                    <td> Civics</td>
                    <td> ***</td>
                    <td> Logic</td>


                </tr>
                <tr class="even pointer">

                    <td >Thursday</td>
                    <td>Logic</td>
                    <td> English</td>
                    <td> Ict </td>
                    <td> Sociology</td>
                    <td> ***</td>
                    <td> ***</td>
                    <td> Social Work </td>
                    <td> ***</td>
                    <td> Bengali</td>


                </tr>


                <tbody>
            </table>

            <div class="clearfix"></div>
            <div class="col-md-7 col-sm-7 col-xs-12 pull-right">
                <a href="pdf.php" class="btn btn-info btn-group-lg"><i class="fa fa-file-pdf-o"></i> Download as pdf</a>
                <a href="email.php?list=1" class="btn btn-warning btn-group-lg"><i class="fa fa-inbox"></i> Send As Email </a>

            </div>
</body>
</html>